import javax.swing.JOptionPane;


public class InputUserName {
	String name;
	
	InputUserName()
	{
		
		name = JOptionPane.showInputDialog("Enter username: ");
	}
	
	public void DisplayName()
	{
		JOptionPane.showMessageDialog(null, "New username created\n\n" + name, "Input UserName", JOptionPane.PLAIN_MESSAGE);
	} 
	
	public static void main(String args[])
	{
		InputUserName user = new InputUserName();
		
		user.DisplayName();
		
		System.exit(0);
	}
	

}
